# Discord.JS Template

This repository aims to provide sane defaults for everything from bot structure to command file templates. All commands and events are handled dynamically. Inspired by Discord.JS-Commando.

This vs Commando:

- Full argument control
- Commands can be in the `commands` folder, or in a subfolder.
- Events folder has discord separated into subdirectory, so events for
other modules can be registered too.
- No messing around with "classes" (purely functional)