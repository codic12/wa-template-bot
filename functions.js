const discord = require("discord.js");

exports.resolveUser = (client, eval) => {
  let cache = client.users.cache;

  // Regex
  let tagRegex = /^(.*)#(?:\d\d\d\d)?$/;
  let mentionRegex = /^<@(\d*)>$/;

  if (typeof eval === "number") {
    return cache.filter(u => u.id === eval)[0];
  } else if (typeof eval === "string") {
    let username = eval.match(tagRegex)[1];
    let id = eval.match(mentionRegex)[1];

    if (username) return cache.filter(u => u.username === username)[0];
    else if (id) return cache.filter(u => u.id === id)[0];
    else {
      let usernamed = cache.filter(
        u => u.username.toLowerCase() === eval.toLowerCase()
      )[0];
      if (usernamed) return usernamed;
      else return null;
    }
  }
};

exports.resolveGuildMember = (guild, eval) => {
  let cache = guild.members.cache;

  // Regex
  let tagRegex = /^(.*)#(?:\d\d\d\d)?$/;
  let mentionRegex = /^<@(\d*)>$/;

  if (typeof eval === "number") {
    return cache.filter(u => u.id === eval)[0];
  } else if (typeof eval === "string") {
    let username = eval.match(tagRegex)[1];
    let id = eval.match(mentionRegex)[1];

    if (username) return cache.filter(u => u.user.username === username)[0];
    else if (id) return cache.filter(u => u.user.id === id)[0];
    else {
      let usernamed = cache.filter(
        u => u.user.username.toLowerCase() === eval.toLowerCase()
      )[0];
      let nicknamed = cache.filter(
        u => u.nickname.toLowerCase() === eval.toLowerCase()
      )[0];
      if (usernamed) return usernamed;
      else if (nicknamed) return nicknamed;
      else return null;
    }
  }
};

exports.resolveChannel = (client, type, eval) => {
  let cache = client.channels.cache;

  let channelType = type ?? "text";
  let typeCache = cache.filter(c => c.type === channelType);

  let regex = /^<#(\d*)>$/;

  if (typeof eval === "number") {
    return typeCache.filter(c => c.id === eval)[0];
  } else if (typeof eval === "string") {
    let id = eval.match(regex)[1];

    if (id) return cache.filter(c => c.id === id)[0];
    else return cache.filter(c => c.name === eval)[0];
  }
};

exports.resolveGuildChannel = (guild, type, eval) => {
  let cache = guild.channels.cache;

  let channelType = type ?? "text";
  let typeCache = cache.filter(c => c.type === channelType);

  let regex = /^<#(\d*)>$/;

  if (typeof eval === "number") {
    return typeCache.filter(c => c.id === eval)[0];
  } else if (typeof eval === "string") {
    let id = eval.match(regex)[1];

    if (id) return cache.filter(c => c.id === id)[0];
    else return cache.filter(c => c.name === eval)[0];
  }
};

exports.webhook = (link) => {
  let regex = /^https:\/\/discordapp.com\/api\/webhooks\/(\d*)\/(\w*)$/;
  let id = link.match(regex)[1];
  let token = link.match(regex)[2];
  let webhook = new discord.WebhookClient(id, token);
  return webhook;
};
