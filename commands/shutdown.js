const discord = require('discord.js');

module.exports = {
    name: 'shutdown',
    description: 'Shuts down the bot.',
    usage: '<delay: seconds>',
    args: false,
    
    guildOnly: false,
    ownerOnly: true,
    async execute(client, message, args) {
        let delay = args[0];

        let shutdown = async(delay) => {
            await new Promise(_ => setTimeout( async () => {
                await message.channel.send(`Shutting down...`);
                client.logger.log('info', 'Shutting down...');
    
                await client.sleep(3);
                
                await message.channel.send(`Shut down complete.`);
                client.logger.log('info', 'Shut down complete.');
                process.exit();
            }, (delay * 1000)));
        }

        if (delay) {
            await message.channel.send(`Shutting down in ${delay} seconds...`);
            client.logger.log('info', `Shutting down in '${delay}' seconds...`)
            await shutdown(delay);
        } else {
            await shutdown(0);
        }
    },
}