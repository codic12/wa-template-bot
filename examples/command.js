const discord = require('discord.js');

module.exports = {
    name: '',
    description: '',
    usage: '',
    args: false, 
    
    guildOnly: false,
    ownerOnly: false,

    userPermissions: [],
    clientPermissions: [],
    execute(client, message, args) { 
        // Code here
    },
}