const discord = require("discord.js");
const config = require("./config.json");
const fs = require("fs");
const winston = require("winston");
const path = require('path');

const client = new discord.Client();
client.config = config;
client.sleep = async (seconds) => await new Promise(r => setTimeout(r, (seconds * 1000)));

// Bot Logging
const timestamp = + new Date();
const logger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: `./logs/${timestamp}.discordbot.log` }),
  ],
  format: winston.format.printf(
    (log) => `[${log.level.toUpperCase()}] - ${log.message}`
  ),
});
client.logger = logger;

// Command Registration
client.commands = new discord.Collection();
const commandPath = path.join(__dirname, 'commands');
const commandFolder = fs.readdirSync(commandPath);
const files = commandFolder.filter(file => file.endsWith('.js'));
const modules = commandFolder.filter(file => fs.statSync(path.join(__dirname, "commands", file)).isDirectory());

if (files.length > 0) {
  for (let file of files) {
    const command = require(path.join(__dirname, 'commands', file));
    client.commands.set(command.name, command);
    client.logger.log('info', `Loaded command file '${command.name}.js'.`);
  }
  client.logger.log('info', 'Loaded all independent commands!');
}

if (modules.length > 0) {
  for (let m of modules) {
    let modulePath = path.join(__dirname, "commands", m);
    let folder = fs.readdirSync(modulePath).filter(file => file.endsWith(".js"));

    for (const file of folder) {
      let filePath = path.join(__dirname, "commands", m, file)
      const command = require(filePath);
      client.commands.set(command.name, command);
    }
    client.logger.log('info', `Loaded module '${m}'.`);
  }
  client.logger.log('info', 'Loaded all modules!');
}

// Event Handler
const eventPath = path.join(__dirname, "events", "discord");
const eventFolder = fs.readdirSync(eventPath);
const eventFiles = eventFolder.filter(file => file.endsWith(".js"));

if (eventFiles.length > 0) {
  for (const file of eventFiles) {
    const eventName = file.split(".")[0];
    const event = require(path.join(__dirname, "events", "discord", file));
  
    client.logger.log('info', `Loaded event file '${eventName}.js'`);
    client.on(eventName, event.bind(null, client));
  }
  client.logger.log('info', 'Loaded all event files!');
}

// Login
client.login(config.bot.token);
