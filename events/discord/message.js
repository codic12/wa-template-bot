const discord = require("discord.js");

// use other arguments, depending on the event
module.exports = (client, message) => {
  let prefix = client.config.bot.prefix;
  const escapeRegex = (str) => str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
  const prefixRegex = new RegExp(
    `^(<@!?${client.user.id}>|${escapeRegex(prefix)})\\s*`
  );
  if (!prefixRegex.test(message.content)) return;

  const [, matchedPrefix] = message.content.match(prefixRegex);

  if (message.author.bot) return;

  const args = message.content.slice(matchedPrefix.length).trim().split(/ +/);
  const commandName = args.shift().toLowerCase();

  const command =
    client.commands.get(commandName) ||
    client.commands.find(
      cmd => cmd.aliases && cmd.aliases.includes(commandName)
    );

  if (!command) return;

  if (command.args && !args.length) {
    let reply = `You didn't provide any arguments, ${message.author}!`;
    if (command.usage) {
      reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
    }
    return message.channel.send(reply);
  }

  if (command.guildOnly) {
    if (message.channel.type !== "text") {
      return message.reply("I can't execute that command in DMs!");
    }
    if (command.clientPermissions) {
      let guildAccount = message.guild.me;
      if (!guildAccount.hasPermission(command.clientPermissions)) {
        return message.channel.send(
          "I do not have permission to run this command!"
        );
      }
    }
    if (command.userPermissions) {
      let member = message.member;
      if (!member.hasPermission(command.userPermissions)) {
        return message.channel.send(
          "You do not have permission to run this command!"
        );
      }
    }
  }

  if (command.ownerOnly) {
    if (!client.config.bot.owners.includes(message.author.id)) {
      return message.channel.send(
        "Insufficent permissions: This command requires a bot owner to run."
      );
    }
  }

  try {
    command.execute(client, message, args);
  } catch (error) {
    logger.log("error", error);
    message.reply("There was an error trying to execute that command.");
  }
};
